export default {
  title: 'Huyi',
  description: 'Trang cá nhân HuYi',
  // canonical: 'https://huyi.tk',
  noindex: true,
  openGraph: {
    // url: 'https://huyi.tk',
    title: 'Trang cá nhân HuYi',
    description: 'Trang cá nhân HuYi',
    images: [
      {
        url: 'https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg',
        width: 1200,
        height: 630,
        alt: 'huyi',
      },
    ],
    locale: 'vi',
    site_name: 'Home',
  },
}
