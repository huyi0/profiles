// import dynamic from 'next/dynamic'
import React, { ReactNode } from 'react'

type Props = {
  children?: ReactNode
}

const HomeLayout = ({ children }: Props) => {
  return <>{children}</>
}

export default HomeLayout
