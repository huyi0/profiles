import React, { ReactNode } from 'react'
type Props = {
  children?: ReactNode
}

const defaultLayout = ({ children }: Props) => {
  return <>{children}</>
}

export default defaultLayout
