const withPlugins = require('next-compose-plugins')
const withImages = require('next-images')
const withReactSvg = require('next-react-svg')
const path = require('path')
const withPWA = require('next-pwa')

const nextConfig = {
  swcMinify: true,
  include: path.resolve(__dirname, 'public/img/svg'),
  images: {
    domains: ['mdbootstrap.com', 'mdbcdn.b-cdn.net'],
  },
  async rewrites() {
    return [
      {
        source: '/robots.txt',
        destination: '/api/robots',
      },
    ]
  },

  webpack(config) {
    return config
  },
}

module.exports = withPlugins([[withImages, withReactSvg]], nextConfig)

module.exports = withPWA({
  pwa: {
    dest: 'public',
    register: true,
    skipWaiting: true,
  },
})
