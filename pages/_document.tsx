import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from 'next/document'
const sprite = require('svg-sprite-loader/runtime/sprite.build')

export default class CustomDocument extends Document<{
  spriteContent: string
}> {
  public static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx)
    const spriteContent = sprite.stringify()

    return {
      spriteContent,
      ...initialProps,
    }
  }

  public render() {
    return (
      <Html lang="vi">
        <Head>
          <link rel="shortcut icon" href="/favicon/huyi.png" />
          <link rel="apple-touch-icon" href="/favicon/huyi.png" />

          <meta charSet="utf-8" />
          <meta content="IE=edge" />

          <meta name="theme-color" content="#317EFB" />
          <link
            rel="preload"
            as="style"
            href="https://fonts.googleapis.com/css?family=Montserrat%3A600%7CRaleway%3A600%7CRaleway%3A700%7CRoboto%20Slab%3A300%7CMontserrat%3A500%7CRoboto%20Slab%3Anormal%7CRaleway%3A500&amp;subset=latin%2Cvietnamese&amp;display=swap"
          ></link>
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Montserrat%3A600%7CRaleway%3A600%7CRaleway%3A700%7CRoboto%20Slab%3A300%7CMontserrat%3A500%7CRoboto%20Slab%3Anormal%7CRaleway%3A500&amp;subset=latin%2Cvietnamese&amp;display=swap"
            media="all"
          ></link>
        </Head>
        <body>
          <div dangerouslySetInnerHTML={{ __html: this.props.spriteContent }} />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
