import PageCV from '@components/organisms/PageCV'
import dynamic from 'next/dynamic'
import React from 'react'
const HomeLayout = dynamic(() => import('templates/home'))

const HomePage = () => {
  return <PageCV />
}

HomePage.Layout = HomeLayout

export default HomePage
