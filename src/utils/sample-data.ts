import { label, slug } from 'src/utils/constants'
export interface menuItem {
  id: string
  slug: string
  label: string
  icon?: string
  srcIcon?: string
  bgImage?: string
}

export interface listMenu extends Array<menuItem> {}

export const listMenu: listMenu = [
  {
    id: slug().GIOI_THIEU,
    slug: slug().GIOI_THIEU,
    label: label().GIOI_THIEU,
    icon: 'fas fa-home',
    srcIcon:
      'https://cdn-icons.flaticon.com/png/512/2549/premium/2549900.png?token=exp=1637093753~hmac=d8270a614a1b10c407293622cad6b119',
    bgImage: '/img/constructive_bg_01.jpg',
  },
  {
    id: slug().SAN_PHAM,
    slug: slug().SAN_PHAM,
    label: label().SAN_PHAM,
    icon: 'fas fa-box-open',
    bgImage: '/img/constructive_bg_02.jpg',
  },
  {
    id: slug().CONG_TY,
    slug: slug().CONG_TY,
    label: label().CONG_TY,
    icon: 'fas fa-smile-wink',
    bgImage: '/img/constructive_bg_03.jpg',
  },
  {
    id: slug().LIEN_HE,
    slug: slug().LIEN_HE,
    label: label().LIEN_HE,
    icon: 'fas fa-comments',
    bgImage: '/img/constructive_bg_04.jpg',
  },
]
