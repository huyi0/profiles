let GIOI_THIEU: string
let CONG_TY: string
let SAN_PHAM: string
let LIEN_HE: string

const slug = () => {
  GIOI_THIEU = 'gioi-thieu'
  CONG_TY = 'cong-ty'
  SAN_PHAM = 'san-pham'
  LIEN_HE = 'lien-he'

  return { GIOI_THIEU, CONG_TY, SAN_PHAM, LIEN_HE }
}

const label = () => {
  GIOI_THIEU = 'Introduce'
  CONG_TY = 'Experience'
  SAN_PHAM = 'Product'
  LIEN_HE = 'Contact'

  return { GIOI_THIEU, CONG_TY, SAN_PHAM, LIEN_HE }
}

export { slug, label }
