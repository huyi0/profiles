import Content from '@components/molecules/Content'
import Sidebar from '@components/molecules/Sidebar'
import { MDBCol, MDBContainer, MDBRow } from 'mdbreact'
import React, { useState } from 'react'
import { slug } from 'src/utils/constants'
import { listMenu } from 'src/utils/sample-data'
import styles from './style.module.scss'

const PageCV = () => {
  const [page, setpage] = useState(slug().GIOI_THIEU)

  const customBgImg = () => {
    const { bgImage }: any = listMenu.find((i) => i.id === page)
    return {
      background: `url(${bgImage})`,
    }
  }
  return (
    <MDBContainer fluid className={styles.Home}>
      <MDBRow className={styles.RowHome} style={customBgImg()}>
        <MDBCol sm="12" md="12" lg="4" xl="3" className={styles.Sidebar}>
          <Sidebar setpage={setpage} />
        </MDBCol>
        <MDBCol sm="12" md="12" lg="8" xl="9" className={styles.Content}>
          <Content page={page} setpage={setpage} />
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  )
}

export default PageCV
