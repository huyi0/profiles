import React from 'react'
import styles from './style.module.scss'
// import Image from 'next/image'

const sanpham = [
  {
    href: 'https://www.bvtrungvuong.vn/',
    src: '/img/sanpham/trungvuong.png',
    description: 'Bệnh viện Trưng vương',
  },
  {
    href: 'https://www.medpro.vn/',
    src: '/img/sanpham/medpro.png',
    description: 'Đăng ký đặt khám trực tuyến Medpro',
  },
  {
    href: 'https://pkdk105.vercel.app/',
    src: '/img/sanpham/pkdk105.png',
    description: 'Phòng khám đa khoa 105',
  },
  {
    href: '#',
    iframe: '/img/sanpham/CV.pdf',
    src: '/img/sanpham/pkdk105.png',
    description: 'My personal information',
  },
]

const SanPham = () => {
  return (
    <section id="tm-section-2" className={styles.sanpham}>
      <div>
        <header className="mb-4">
          <h2 className="tm-text-shadow">Our Products</h2>
        </header>
        <div className={styles.container}>
          <ul className={styles.slider}>
            {sanpham.map((v, i) => {
              return (
                <li key={i}>
                  <a
                    href={v.src}
                    target="_blank"
                    className="tm-slider-img"
                    rel="noreferrer"
                  >
                    {v.iframe ? (
                      <iframe
                        src={v.iframe}
                        scrolling="false"
                        frameBorder="0"
                      />
                    ) : (
                      <img src={v.src} alt="Image" />
                    )}
                  </a>
                  <div className={styles.description}>
                    <p>{v.description}</p>
                  </div>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
    </section>
  )
}

export default SanPham
