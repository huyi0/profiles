import { MDBCard, MDBCardBody, MDBCardHeader, MDBCardText } from 'mdbreact'
import React from 'react'
import { slug } from 'src/utils/constants'
import styles from './style.module.scss'

interface props {
  page?: string
  setpage?: any
}
const GioiThieu = ({ setpage }: props) => {
  const selectedPage = (el: string) => {
    setpage(el)
  }
  return (
    <section id="tm-section-1" className={styles.section}>
      <MDBCard className={styles.card}>
        <MDBCardHeader tag="h1" className={styles.header}>
          And this is me - Huyi ...
        </MDBCardHeader>
        <MDBCardBody className={styles.body}>
          <MDBCardText className={styles.text}>
            {' '}
            <b className="text-warning">Info</b> : <br />
            (fullname: Đỗ Hửu Huy - born 1998) I have had a passion for
            programming and website design since I was in high school.
            <br />I have almost 1 year of experience, and I want best FE reactjs
            + nextjs Fullstack with node, BE is nestjs
            <br />
            <br />
            Every day is an adventure a small happiness, a big dream !
            <br />
            <br />
            <b className="text-warning">Skill</b> : <br />
            Cms strapi, Reactjs, nextjs, saga, thunk, persist, webpack, eslint,
            build source, less, sass, antd, mdbreact, boostrap Linux, centos,
            docker, pm2, git, gitlab Teamwork, jira, trello Node, Express,
            nestjs, mongo
          </MDBCardText>

          <button
            className={styles.button}
            onClick={() => selectedPage(slug().SAN_PHAM)}
          >
            See more
          </button>
        </MDBCardBody>
      </MDBCard>
    </section>
  )
}

export default GioiThieu
