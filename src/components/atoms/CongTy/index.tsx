import { MDBCol, MDBRow } from 'mdbreact'
import React from 'react'
import cx from 'classnames'
import styles from './style.module.scss'

const listCongTy = [
  {
    icon: 'https://resource.medpro.com.vn/static/images/medpro/web/footer_logo.svg?t=87054.84374738496',
    label: 'Công ty Cổ phần Ứng dụng Medpro ( > 1 years work )',
    description: (
      <>
        <p>
          Chuyên về cung cấp giải pháp y tế cho bệnh viện, phòng khám, phòng
          mạch
        </p>
        <a href="https://medpro.vn/">Xem thêm: https://medpro.vn/</a>
      </>
    ),
  },
  {
    icon: 'https://cdn-icons-png.flaticon.com/512/4298/4298932.png',
    label: 'Đang cập nhật ...',
    description: `We provide a variety of templates for you at no cost. Please
                    spread a word about website. Thank you.`,
  },
  {
    icon: 'https://cdn-icons-png.flaticon.com/512/4298/4298932.png',

    label: 'Đang cập nhật ...',
    description: `We provide a variety of templates for you at no cost. Please
                    spread a word about website. Thank you.`,
  },
  {
    icon: 'https://cdn-icons-png.flaticon.com/512/4298/4298932.png',

    label: 'Đang cập nhật ...',
    description: `We provide a variety of templates for you at no cost. Please
                    spread a word about website. Thank you.`,
  },
]
const CongTy = () => {
  return (
    <section id="tm-section-3" className="tm-section">
      <MDBRow className="mb-4">
        <MDBCol xl="12">
          <h2 className="tm-text-shadow">Our Company</h2>
        </MDBCol>
      </MDBRow>
      <MDBRow>
        <MDBCol>
          <ul className={styles.listCongTy}>
            {listCongTy.map(({ label, description, icon }, index: any) => {
              return (
                <li className={styles.media} key={index}>
                  <div className={styles.card}>
                    <div className={styles.cardHeader}>
                      <figure className={cx(styles.icon_media)}>
                        <img src={icon} alt="" />
                      </figure>
                      <p className={styles.title}>{label}</p>
                    </div>
                    <div className={styles.cardBody}>{description}</div>
                  </div>
                </li>
              )
            })}{' '}
          </ul>
        </MDBCol>
      </MDBRow>
    </section>
  )
}

export default CongTy
