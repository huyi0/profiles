import CongTy from '@components/atoms/CongTy'
import GioiThieu from '@components/atoms/GioiThieu'
import LienHe from '@components/atoms/LienHe'
import SanPham from '@components/atoms/SanPham'
import React from 'react'
import { slug } from 'src/utils/constants'
const type = slug()

interface props {
  page?: string
  setpage?: any
}

const Content = ({ page, setpage }: props) => {
  switch (page) {
    case type.GIOI_THIEU:
      return <GioiThieu setpage={setpage} />

    case type.SAN_PHAM:
      return <SanPham />

    case type.CONG_TY:
      return <CongTy />

    case type.LIEN_HE:
      return <LienHe />

    default:
      return null
  }
}

export default Content
