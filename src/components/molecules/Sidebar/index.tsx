import React from 'react'
import { listMenu, menuItem } from 'src/utils/sample-data'
import styles from './style.module.scss'
import cx from 'classnames'

interface props {
  setpage?: any
}

const Sidebar = ({ setpage }: props) => {
  const selectedPage = (el: string) => () => {
    setpage(el)
  }

  return (
    <div className={styles.inSidebar}>
      <ul className={styles.main_nav}>
        {listMenu.map(({ label, icon, srcIcon, id }: menuItem, index: any) => {
          return (
            <li key={index} onClick={selectedPage(id)}>
              <a href="#">
                <figure className={cx(styles.customIcon)}>
                  {icon ? <i className={icon} /> : <img src={srcIcon} alt="" />}
                </figure>

                <span>{label}</span>
              </a>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default Sidebar
