import React from 'react'

const Footer = () => {
  return (
    <footer className="footer-link">
      <p className="tm-copyright-text">
        Copyright © 2018 Constructive Co. Ltd. - Design: huyi
      </p>
    </footer>
  )
}

export default Footer
